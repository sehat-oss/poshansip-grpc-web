/**
 * @fileoverview gRPC-Web generated client stub for protohansip
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.protohansip = require('./token_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.protohansip.TokenClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.protohansip.TokenPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.protohansip.ValidateTokenRequest,
 *   !proto.protohansip.ValidateTokenResponse>}
 */
const methodDescriptor_Token_Validate = new grpc.web.MethodDescriptor(
  '/protohansip.Token/Validate',
  grpc.web.MethodType.UNARY,
  proto.protohansip.ValidateTokenRequest,
  proto.protohansip.ValidateTokenResponse,
  /**
   * @param {!proto.protohansip.ValidateTokenRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.ValidateTokenResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.protohansip.ValidateTokenRequest,
 *   !proto.protohansip.ValidateTokenResponse>}
 */
const methodInfo_Token_Validate = new grpc.web.AbstractClientBase.MethodInfo(
  proto.protohansip.ValidateTokenResponse,
  /**
   * @param {!proto.protohansip.ValidateTokenRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.ValidateTokenResponse.deserializeBinary
);


/**
 * @param {!proto.protohansip.ValidateTokenRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.protohansip.ValidateTokenResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.protohansip.ValidateTokenResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.protohansip.TokenClient.prototype.validate =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/protohansip.Token/Validate',
      request,
      metadata || {},
      methodDescriptor_Token_Validate,
      callback);
};


/**
 * @param {!proto.protohansip.ValidateTokenRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.protohansip.ValidateTokenResponse>}
 *     A native promise that resolves to the response
 */
proto.protohansip.TokenPromiseClient.prototype.validate =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/protohansip.Token/Validate',
      request,
      metadata || {},
      methodDescriptor_Token_Validate);
};


module.exports = proto.protohansip;

