/**
 * @fileoverview gRPC-Web generated client stub for protohansip
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.protohansip = require('./account_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.protohansip.AccountClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.protohansip.AccountPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.protohansip.CreateAccountRequest,
 *   !proto.protohansip.AccountResponse>}
 */
const methodDescriptor_Account_Create = new grpc.web.MethodDescriptor(
  '/protohansip.Account/Create',
  grpc.web.MethodType.UNARY,
  proto.protohansip.CreateAccountRequest,
  proto.protohansip.AccountResponse,
  /**
   * @param {!proto.protohansip.CreateAccountRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.AccountResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.protohansip.CreateAccountRequest,
 *   !proto.protohansip.AccountResponse>}
 */
const methodInfo_Account_Create = new grpc.web.AbstractClientBase.MethodInfo(
  proto.protohansip.AccountResponse,
  /**
   * @param {!proto.protohansip.CreateAccountRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.AccountResponse.deserializeBinary
);


/**
 * @param {!proto.protohansip.CreateAccountRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.protohansip.AccountResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.protohansip.AccountResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.protohansip.AccountClient.prototype.create =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/protohansip.Account/Create',
      request,
      metadata || {},
      methodDescriptor_Account_Create,
      callback);
};


/**
 * @param {!proto.protohansip.CreateAccountRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.protohansip.AccountResponse>}
 *     A native promise that resolves to the response
 */
proto.protohansip.AccountPromiseClient.prototype.create =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/protohansip.Account/Create',
      request,
      metadata || {},
      methodDescriptor_Account_Create);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.protohansip.ChangeAccountStatusRequest,
 *   !proto.protohansip.AccountResponse>}
 */
const methodDescriptor_Account_ChangeStatus = new grpc.web.MethodDescriptor(
  '/protohansip.Account/ChangeStatus',
  grpc.web.MethodType.UNARY,
  proto.protohansip.ChangeAccountStatusRequest,
  proto.protohansip.AccountResponse,
  /**
   * @param {!proto.protohansip.ChangeAccountStatusRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.AccountResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.protohansip.ChangeAccountStatusRequest,
 *   !proto.protohansip.AccountResponse>}
 */
const methodInfo_Account_ChangeStatus = new grpc.web.AbstractClientBase.MethodInfo(
  proto.protohansip.AccountResponse,
  /**
   * @param {!proto.protohansip.ChangeAccountStatusRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.AccountResponse.deserializeBinary
);


/**
 * @param {!proto.protohansip.ChangeAccountStatusRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.protohansip.AccountResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.protohansip.AccountResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.protohansip.AccountClient.prototype.changeStatus =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/protohansip.Account/ChangeStatus',
      request,
      metadata || {},
      methodDescriptor_Account_ChangeStatus,
      callback);
};


/**
 * @param {!proto.protohansip.ChangeAccountStatusRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.protohansip.AccountResponse>}
 *     A native promise that resolves to the response
 */
proto.protohansip.AccountPromiseClient.prototype.changeStatus =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/protohansip.Account/ChangeStatus',
      request,
      metadata || {},
      methodDescriptor_Account_ChangeStatus);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.protohansip.ChangePasswordRequest,
 *   !proto.protohansip.AccountResponse>}
 */
const methodDescriptor_Account_ChangePassword = new grpc.web.MethodDescriptor(
  '/protohansip.Account/ChangePassword',
  grpc.web.MethodType.UNARY,
  proto.protohansip.ChangePasswordRequest,
  proto.protohansip.AccountResponse,
  /**
   * @param {!proto.protohansip.ChangePasswordRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.AccountResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.protohansip.ChangePasswordRequest,
 *   !proto.protohansip.AccountResponse>}
 */
const methodInfo_Account_ChangePassword = new grpc.web.AbstractClientBase.MethodInfo(
  proto.protohansip.AccountResponse,
  /**
   * @param {!proto.protohansip.ChangePasswordRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.AccountResponse.deserializeBinary
);


/**
 * @param {!proto.protohansip.ChangePasswordRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.protohansip.AccountResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.protohansip.AccountResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.protohansip.AccountClient.prototype.changePassword =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/protohansip.Account/ChangePassword',
      request,
      metadata || {},
      methodDescriptor_Account_ChangePassword,
      callback);
};


/**
 * @param {!proto.protohansip.ChangePasswordRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.protohansip.AccountResponse>}
 *     A native promise that resolves to the response
 */
proto.protohansip.AccountPromiseClient.prototype.changePassword =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/protohansip.Account/ChangePassword',
      request,
      metadata || {},
      methodDescriptor_Account_ChangePassword);
};


module.exports = proto.protohansip;

