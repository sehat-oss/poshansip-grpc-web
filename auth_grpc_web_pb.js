/**
 * @fileoverview gRPC-Web generated client stub for protohansip
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.protohansip = require('./auth_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.protohansip.AuthClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.protohansip.AuthPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.protohansip.AuthenticationRequest,
 *   !proto.protohansip.AuthenticatitonResponse>}
 */
const methodDescriptor_Auth_Login = new grpc.web.MethodDescriptor(
  '/protohansip.Auth/Login',
  grpc.web.MethodType.UNARY,
  proto.protohansip.AuthenticationRequest,
  proto.protohansip.AuthenticatitonResponse,
  /**
   * @param {!proto.protohansip.AuthenticationRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.AuthenticatitonResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.protohansip.AuthenticationRequest,
 *   !proto.protohansip.AuthenticatitonResponse>}
 */
const methodInfo_Auth_Login = new grpc.web.AbstractClientBase.MethodInfo(
  proto.protohansip.AuthenticatitonResponse,
  /**
   * @param {!proto.protohansip.AuthenticationRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.AuthenticatitonResponse.deserializeBinary
);


/**
 * @param {!proto.protohansip.AuthenticationRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.protohansip.AuthenticatitonResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.protohansip.AuthenticatitonResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.protohansip.AuthClient.prototype.login =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/protohansip.Auth/Login',
      request,
      metadata || {},
      methodDescriptor_Auth_Login,
      callback);
};


/**
 * @param {!proto.protohansip.AuthenticationRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.protohansip.AuthenticatitonResponse>}
 *     A native promise that resolves to the response
 */
proto.protohansip.AuthPromiseClient.prototype.login =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/protohansip.Auth/Login',
      request,
      metadata || {},
      methodDescriptor_Auth_Login);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.protohansip.LogoutRequest,
 *   !proto.protohansip.LogoutResponse>}
 */
const methodDescriptor_Auth_Logout = new grpc.web.MethodDescriptor(
  '/protohansip.Auth/Logout',
  grpc.web.MethodType.UNARY,
  proto.protohansip.LogoutRequest,
  proto.protohansip.LogoutResponse,
  /**
   * @param {!proto.protohansip.LogoutRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.LogoutResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.protohansip.LogoutRequest,
 *   !proto.protohansip.LogoutResponse>}
 */
const methodInfo_Auth_Logout = new grpc.web.AbstractClientBase.MethodInfo(
  proto.protohansip.LogoutResponse,
  /**
   * @param {!proto.protohansip.LogoutRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.protohansip.LogoutResponse.deserializeBinary
);


/**
 * @param {!proto.protohansip.LogoutRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.protohansip.LogoutResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.protohansip.LogoutResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.protohansip.AuthClient.prototype.logout =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/protohansip.Auth/Logout',
      request,
      metadata || {},
      methodDescriptor_Auth_Logout,
      callback);
};


/**
 * @param {!proto.protohansip.LogoutRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.protohansip.LogoutResponse>}
 *     A native promise that resolves to the response
 */
proto.protohansip.AuthPromiseClient.prototype.logout =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/protohansip.Auth/Logout',
      request,
      metadata || {},
      methodDescriptor_Auth_Logout);
};


module.exports = proto.protohansip;

